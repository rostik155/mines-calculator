# General info

**Techies mines calculation tool**

Online tool for fast mines damage calculation to explode particular enemy

Bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

DEMO: https://mines.gl.rostik.pro/

## Tools required

 - NodeJs 10.16+ (LTS is recommended)
 - NPM 5.2+ (LTS is recommended) or Yarn

## Running

Run following commands in project's root direcory:

*NPM:*
``` bash
npm start
```

*Yarn:*
``` bash
yarn start
```

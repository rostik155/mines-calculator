import React, {useEffect, useState} from 'react';
import './App.css';
import {calculateDamage, CalculateDamageProps, hasUndefinedProps} from "./utils";
import {useTranslation} from "react-i18next";

interface TotalDamageProps extends CalculateDamageProps {
  hitPoints?: number,
}

interface MinesCount {
  proximity: number;
  remote: number;
}

function TotalDamage(props: TotalDamageProps) {
  const { t } = useTranslation()
  const [requiredMines, setRequiredMines] = useState<MinesCount>({
    proximity: 0,
    remote: 0,
  });
  const valid = !hasUndefinedProps((props));

  useEffect(() => {
    if (!valid) return;
    let damage = 0;
    let proximity = 0;
    let remote = 0;
    if (props.useProximity) {
      damage += calculateDamage({useProximity: true, proximityLvl: props.proximityLvl, resistance: props.resistance});
      proximity = 1;
    }
    while (damage < (props.hitPoints || 0)) {
      damage += calculateDamage({
        remoteLvl: props.remoteLvl,
        hasAghanim: props.hasAghanim,
        remotesCount: 1,
        resistance: props.resistance
      });
      remote += 1;
    }
    setRequiredMines({proximity, remote});
  }, [
    props.proximityLvl,
    props.remoteLvl,
    props.hasAghanim,
    props.useProximity,
    props.remotesCount,
    props.resistance,
    props.hitPoints,
    valid
  ])

  return (
    <div className="required-mines card blue lighten-5">
      <div className="card-content">
        {valid ? <>
          {t("Required mines")}:
          <span className="green-text text-darken-3"> {t('N remote mines', {count: requiredMines.remote})} </span>
          {(requiredMines.proximity &&
            <>{t("and")} <span className="red-text text-darken-3">{t('1 proximity mine')}</span></>) || ''}
        </> : <>
          {t("Fill all fields to obtain results")}
        </>}
      </div>
    </div>
  );
}

export default TotalDamage;

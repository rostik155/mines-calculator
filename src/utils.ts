import React from "react";

interface MinMax {
  min?: number,
  max?: number,
}

export const formatters = {
  range: ({ min, max }: MinMax) => (val: number): number => {
    if (min !== undefined && max !== undefined && min >= max) {
      throw new Error(`min (${min}) has to be greater than max (${max})`);
    }
    if (min !== undefined) {
      val = Math.max(val, min);
    }
    if (max !== undefined) {
      val = Math.min(val, max);
    }
    return val;
  }
}

export const getInputHandler = (targetHandler: Function, formatter: Function = (v: any) => v) =>
  (event: React.ChangeEvent<HTMLInputElement>) => {
    const {target} = event;
    let baseValue;
    switch (target.type) {
      case 'text':
        return targetHandler(formatter(target.value));
      case 'number':
        baseValue = target.value;
        if (baseValue === '') {
          return targetHandler(undefined)
        }
        return targetHandler(formatter(+target.value));
      case 'checkbox':
        return targetHandler(formatter(target.checked));
    }
  };

export interface CalculateDamageProps {
  proximityLvl?: number,
  remoteLvl?: number,
  hasAghanim?: boolean,
  useProximity?: boolean,
  remotesCount?: number,
  resistance?: number,
}

const proximityDamage = [200, 380, 560, 740];
const remotesDamage = [300, 450, 600];
const remotesWithAghanimDamage = [450, 600, 750];
export const defaultCalculateDamageProps: Required<CalculateDamageProps> = {
  proximityLvl: 1,
  remoteLvl: 1,
  hasAghanim: false,
  useProximity: false,
  remotesCount: 0,
  resistance: 0,
}

type ObjectOfAny = {[key: string]: any};
type ObjectOfNumbersOrBool = {[key: string]: number|boolean};
function filterNumbers(obj: ObjectOfAny): ObjectOfNumbersOrBool{
  return Object.keys(obj).reduce<ObjectOfNumbersOrBool>((result: ObjectOfNumbersOrBool, key) => {
    if (Number.isInteger(obj[key]) || typeof obj[key] === 'boolean') {
      result[key] = obj[key];
    }
    return result;
  }, {})
}

export function calculateDamage(props: CalculateDamageProps) {
  const allProps = {...defaultCalculateDamageProps, ...filterNumbers(props)};
  let damage = 0;
  if (allProps.useProximity) {
    damage += proximityDamage[allProps.proximityLvl - 1];
  }
  damage += (allProps.hasAghanim ? remotesWithAghanimDamage : remotesDamage)[allProps.remoteLvl - 1] * allProps.remotesCount;
  return damage * ((100 - allProps.resistance) / 100);
}

export function hasUndefinedProps(props: ObjectOfAny): boolean {
  return Object.values(props).findIndex(prop => prop === undefined) !== -1;
}

export function formatNumberValue(val?: number) {
  return val === undefined ? '' : `${val}`;
}

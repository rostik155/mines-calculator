import React, {useEffect, useState} from 'react';
import './App.css';
import {calculateDamage, CalculateDamageProps, hasUndefinedProps} from "./utils";
import {useTranslation} from "react-i18next";

interface DamageInfoProps extends CalculateDamageProps {
  hitPoints?: number,
}

function TotalDamage(props: DamageInfoProps) {
  const { t } = useTranslation()
  const [totalDamage, setTotalDamage] = useState(0);
  const valid = !hasUndefinedProps((props));

  useEffect(() => {
    if (valid) setTotalDamage(calculateDamage(props));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    props.proximityLvl,
    props.remoteLvl,
    props.hasAghanim,
    props.useProximity,
    props.remotesCount,
    props.resistance,
    valid
  ]);

  return (
    <div className="card blue lighten-5">
      <div className="card-content">
        {valid ? <>
          {t("Total damage, considering the resistance")}: <b>{totalDamage}</b>
        </> : <>
          {t("Fill all fields to obtain results")}
        </>}
      </div>
    </div>
  );
}

export default TotalDamage;

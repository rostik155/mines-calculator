import React, {useCallback} from 'react';
import './App.css';
import {formatNumberValue, formatters, getInputHandler} from "./utils";
import {useTranslation} from "react-i18next";

interface TechiesInfoProps {
  resistance?: number,
  hitPoints?: number,
  setResistance: (resistance: number) => void,
  setHitPoints: (hitPoints: number) => void,
}

function EnemyInfo(props: TechiesInfoProps) {
  const { t } = useTranslation()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const resistanceHandler = useCallback(getInputHandler(props.setResistance, formatters.range({max: 99})), [props.setResistance]);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const hitPointsHandler = useCallback(getInputHandler(props.setHitPoints, formatters.range({min: 0})), [props.setHitPoints]);

  return (
    <div className="enemy-info">
      <h5>{t("Enemy Info")}:</h5>
      <div className="row">
        <div className="input-field col s4">
          <input
            id="resistance"
            type="number"
            value={formatNumberValue(props.resistance)}
            onChange={resistanceHandler}
            min={0}
          />
          <label htmlFor="resistance">{t("Resistance")}:</label>
        </div>
        <div className="input-field col s4">
          <input
            id="hitPoints"
            type="number"
            value={formatNumberValue(props.hitPoints)}
            onChange={hitPointsHandler}
            min={0}
          />
          <label htmlFor="hitPoints">{t("HP")}:</label>
        </div>
      </div>
    </div>
  );
}

export default EnemyInfo;

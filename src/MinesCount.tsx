import React, {useCallback} from 'react';
import './App.css';
import {formatNumberValue, formatters, getInputHandler} from "./utils";
import {useTranslation} from "react-i18next";

interface TechiesInfoProps {
  useProximity: boolean,
  remotesCount?: number,

  setUseProximity: (useProximity: boolean) => void,
  setRemotesCount: (remotesCount: number) => void,
}

function MinesCount(props: TechiesInfoProps) {
  const { t } = useTranslation()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const useProximityHandler = useCallback(getInputHandler(props.setUseProximity), [props.setUseProximity]);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const remotesCountHandler = useCallback(getInputHandler(props.setRemotesCount, formatters.range({min: 0})), [props.setRemotesCount]);

  return (
    <div className="mines-count">
      <h5>{t("Mines count")}:</h5>
      <div className="row">
        <div className="input-field col s4">
          <label>
            <input
              type="checkbox"
              checked={props.useProximity}
              onChange={useProximityHandler}
            />
            <span>{t("Set one proximity mine")}</span>
          </label>
        </div>
        <div className="input-field col s4">
          <input
            id="remotesCount"
            type="number"
            value={formatNumberValue(props.remotesCount)}
            onChange={remotesCountHandler}
            min={0}
          />
          <label htmlFor="remotesCount">{t("Remotes count")}:</label>
        </div>
      </div>
    </div>
  );
}

export default MinesCount;

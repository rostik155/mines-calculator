import i18n, {TFunction} from 'i18next'
import { initReactI18next } from 'react-i18next'
import en from './locales/en.json'
import ru from './locales/ru.json'

interface Resources {
  [key: string]: {
    translation: object,
    name: string,
  }
}

const DEFAULT_LOCALE = 'en' as const;

export const resources: Resources = {
  en: { translation: en, name: 'English' },
  ru: { translation: ru, name: 'Русский' },
};

export async function init(): Promise<TFunction> {
  return await i18n.use(initReactI18next).init({
    lng: getUserLocale(),
    resources,
  });
}

export function getUserLocale(): string {
  const selectedLocale = localStorage.getItem('locale');
  let locales = selectedLocale ? [selectedLocale] : null;
  if (!locales) {
    locales = getBrowserLocales({languageCodeOnly: true});
  }
  const userLocale = locales.find(locale => !!resources[locale]);
  return userLocale || DEFAULT_LOCALE;
}

interface GetBrowserLocalesOptions {
  languageCodeOnly?: boolean
}

const defaultGetBrowserLocalesOptions: GetBrowserLocalesOptions = {
  languageCodeOnly: false,
};

function getBrowserLocales(options: GetBrowserLocalesOptions = {}): string[] {
  const opt = {
    ...defaultGetBrowserLocalesOptions,
    ...options,
  };

  const browserLocales =
    navigator.languages === undefined
      ? [navigator.language]
      : navigator.languages;

  if (!browserLocales) {
    return [];
  }

  return browserLocales.map(locale => {
    const trimmedLocale = locale.trim();

    return opt.languageCodeOnly
      ? trimmedLocale.split(/[-_]/)[0]
      : trimmedLocale;
  });
}
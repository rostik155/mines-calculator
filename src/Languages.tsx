import {MouseEvent, useCallback, useEffect, useState} from "react";
import i18n from 'i18next'
import {resources} from "./translations";

import "./Languages.css";

declare const window: Window & typeof globalThis & {
  M: {
    Dropdown: {
      init: (elements: NodeListOf<Element>) => unknown,
    }
  }
};

const Languages = () => {
  useEffect(() => {
    const elems = document.querySelectorAll('.dropdown-trigger');
    window.M.Dropdown.init(elems);
  });

  const [currentLocale, setCurrentLocale] = useState<string>(i18n.language)

  useEffect(() => {
    i18n.changeLanguage(currentLocale)
    localStorage.setItem('locale', currentLocale);
  }, [currentLocale])

  const onLocaleChanged = useCallback((e: MouseEvent<HTMLUListElement>) => {
    if (!(e.target instanceof HTMLSpanElement)) {
      return;
    }
    const locale = e.target.dataset.locale;
    if (locale) {
      setCurrentLocale(locale);
    }
  }, []);

  return <div className="languages">
    <span className='dropdown-trigger btn' data-target='dropdown'>
      {resources[currentLocale]?.name || currentLocale}
    </span>
    <ul id="dropdown" className='dropdown-content' onClick={onLocaleChanged}>
      {Object.keys(resources).map((langId) => <li>
        <span data-locale={langId}>{resources[langId].name}</span>
      </li>)}
    </ul>
  </div>
};

export default Languages;

import React, { useState } from 'react';
import './App.css';
import TechiesInfo from "./TechiesInfo";
import TotalDamage from "./TotalDamage";
import EnemiesInfo from "./EnemyInfo";
import {Gitlab} from "./icons";
import MinesCount from "./MinesCount";
import RequiredMines from "./RequiredMines";
import Languages from "./Languages";
import { useTranslation } from 'react-i18next'

function App() {
  const { t } = useTranslation()
  const [proximityLvl, setProximityLvl] = useState<number|undefined>(4);
  const [remoteLvl, setRemoteLvl] = useState<number|undefined>(1);
  const [hasAghanim, setHasAghanim] = useState<boolean>(false);
  const [useProximity, setUseProximity] = useState<boolean>(true);
  const [remotesCount, setRemotesCount] = useState<number|undefined>(0);
  const [resistance, setResistance] = useState<number|undefined>(25);
  const [hitPoints, setHitPoints] = useState<number|undefined>(1000);

  return (
    <div className="App container">
      <Languages />
      <h1 className="pink-text text-darken-2">
        {t("Techies mines calculation tool")}
      </h1>
      <TechiesInfo
        proximityLvl={proximityLvl}
        remoteLvl={remoteLvl}
        hasAghanim={hasAghanim}
        setProximityLvl={setProximityLvl}
        setRemoteLvl={setRemoteLvl}
        setHasAghanim={setHasAghanim}
      />
      <MinesCount
        useProximity={useProximity}
        remotesCount={remotesCount}
        setUseProximity={setUseProximity}
        setRemotesCount={setRemotesCount}
      />
      <TotalDamage
        proximityLvl={proximityLvl}
        remoteLvl={remoteLvl}
        hasAghanim={hasAghanim}
        useProximity={useProximity}
        remotesCount={remotesCount}
        hitPoints={hitPoints}
        resistance={resistance}
      />
      <EnemiesInfo
        resistance={resistance}
        hitPoints={hitPoints}
        setResistance={setResistance}
        setHitPoints={setHitPoints}
      />
      <RequiredMines
        proximityLvl={proximityLvl}
        remoteLvl={remoteLvl}
        hasAghanim={hasAghanim}
        useProximity={useProximity}
        remotesCount={remotesCount}
        hitPoints={hitPoints}
        resistance={resistance}
      />
      <footer>
        <a href="https://gitlab.com/rostik155/mines-calculator" target="_blank" rel="noreferrer">
          <span>{t("Contribute to the project on")}</span>
          {Gitlab({
            width: '80px',
          })}
        </a>
      </footer>
    </div>
  );
}

export default App;

import React, {useCallback} from 'react';
import './App.css';
import {formatNumberValue, formatters, getInputHandler} from "./utils";
import {useTranslation} from "react-i18next";

interface TechiesInfoProps {
  proximityLvl?: number,
  remoteLvl?: number,
  hasAghanim: boolean,

  setProximityLvl: (proximityLvl: number) => void,
  setRemoteLvl: (remoteLvl: number) => void,
  setHasAghanim: (hasAghanim: boolean) => void,
}

function TechiesInfo(props: TechiesInfoProps) {
  const { t } = useTranslation()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const proximityLvlHandler = useCallback(getInputHandler(props.setProximityLvl, formatters.range({min: 1, max: 4})), [props.setProximityLvl]);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const remoteLvlHandler = useCallback(getInputHandler(props.setRemoteLvl, formatters.range({min: 1, max: 3})), [props.setRemoteLvl]);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const hasAghanimHandler = useCallback(getInputHandler(props.setHasAghanim), [props.setHasAghanim]);

  return (
    <div className="techies-info">
      <h5>{t("Techies Info")}:</h5>
      <div className="row">
        <div className="input-field col s4">
          <input
            id="proximityLvl"
            type="number"
            value={formatNumberValue(props.proximityLvl)}
            onChange={proximityLvlHandler}
            min={0}
            max={4}
          />
          <label htmlFor="proximityLvl">{t("Proximity mines lvl")}:</label>
        </div>
        <div className="input-field col s4">
          <input
            id="remoteLvl"
            type="number"
            value={formatNumberValue(props.remoteLvl)}
            onChange={remoteLvlHandler}
            min={0}
            max={3}
          />
          <label htmlFor="remoteLvl">{t("Remote mines lvl")}:</label>
        </div>
        <div className="input-field col s4">
          <label>
            <input
              type="checkbox"
              checked={props.hasAghanim}
              onChange={hasAghanimHandler}
            />
            <span>{t("Has Aghanim's Scepter")}</span>
          </label>
        </div>
      </div>
    </div>
  );
}

export default TechiesInfo;
